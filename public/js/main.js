$(document).ready(function () {
    $(".help-btn").on("click", function() {
        if ($(".mobile-overlay-help").hasClass("active")) {
            $(".mobile-overlay-help").removeClass("active");
            $(this).find("i").toggleClass("fa-question-circle fa-times");
        } else {
            $(".mobile-overlay-help").addClass("active");
            $(this).find("i").toggleClass("fa-question-circle fa-times");
        }
    });
});