var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var MovieSchema = new Schema(
  {
    title: {type: String, required: true, minlength: 1, maxlength: 50},
    format: {type: String, required: true, enum: ['VHS', 'vhs', 'DVD', 'dvd', 'Streaming', 'streaming'], default: 'Streaming'},
    length: {type: Number, required: true, min: 0, max: 500},
    release_year: {type: Number, required: true, min: 1800, max: 2100},
    rating: {type: Number, required: true, min: 1, max: 5},
  }
);

// Virtual for movie's URL
MovieSchema
.virtual('url')
.get(function () {
  return '/movie/' + this._id;
});

// Export model
module.exports = mongoose.model('Movie', MovieSchema);