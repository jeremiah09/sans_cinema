var Movie = require('../models/movie');
// var async = require('async');
const { body, validationResult, sanitizeBody } = require('express-validator');

// Display list of all Movies.
exports.movie_list = function(req, res, next) {
    Movie.find({}, 'title format length release_year rating', function(e, docs) {
        if (e) {
            return next(e);
        }
        // Success
        res.render('movie_list', { title: 'Movie List', movies: docs, url: req.originalUrl });
    });
};

// Display detail page for a specific Movie.
exports.movie_detail = function(req, res, next) {
    Movie.findById(req.params.id, function(e, docs) {
        if (e) {
            return next(e);
        }
        // Success
        res.render('movie_detail', { title: 'Movie Details', movie: docs });
    });
};

// Display Movie create form on GET.
exports.movie_create_get = function(req, res, next) {
    res.render('movie_form', { title: 'Create Movie' });
};

// Handle Movie create on POST.
exports.movie_create_post = [
    // Validate fields.
    body('title').isLength({ min: 1, max: 50 }).trim().withMessage('Title must be between 1-50 characters.'),
    body('format').isIn(['VHS', 'vhs', 'DVD', 'dvd', 'Streaming', 'streaming']).withMessage('Format must be one of the following: VHS, DVD, or Streaming.'),
    body('length').isInt({ min: 0, max: 500}).trim().withMessage('Length must be a value between 0 - 500.'),
    body('release_year').isInt({ min: 1800, max: 2100 }).trim().withMessage('Release Year must must be a value between 1800 - 2100.'),
    body('rating').isInt({ min: 1, max: 5 }).trim().withMessage('Rating must be a value between 1 - 5.'),

    // Sanitize fields.
    sanitizeBody('title').escape(),
    sanitizeBody('format').escape(),
    sanitizeBody('length').escape(),
    sanitizeBody('release_year').escape(),
    sanitizeBody('rating').escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a Movie object with escaped and trimmed data.
        var movie = new Movie(
            {
                title: req.body.title,
                format: req.body.format,
                length: req.body.length,
                release_year: req.body.release_year,
                rating: req.body.rating
            });

        if (!errors.isEmpty()) {
            // There are errors. Render form again with sanitized values/errors messages.
            res.render('movie_form', { title: 'Create Movie', movie: req.body, errors: errors.array() });
            return;
        }
        else {
            // Form is valid.
            movie.save(function (e) {
                if (e) { 
                    return next(e); 
                }
                // Success - redirect to new movie record.
                res.redirect(movie.url);
            });
        }
    }
];

// Display Movie delete form on GET.
exports.movie_delete_get = function(req, res) {
    Movie.findById(req.params.id, function(e, docs) {
        if (e) {
            return next(e);
        }
        if (docs==null) {
            res.redirect('/movies');
        }
        // Success
        res.render('movie_delete', { title: 'Delete Movie', movie: docs });
    });
};

// Handle Movie delete on POST.
exports.movie_delete_post = function(req, res) {
    Movie.findByIdAndRemove(req.body.id, function deleteMovie(e) {
        if (e) { 
            return next(e); 
        }
        // Success, redirect to list of movies.
        res.redirect('/movies');
    });
};

// Display Movie update form on GET.
exports.movie_update_get = function(req, res, next) {
    Movie.findById(req.params.id, function(e, docs) {
        if (e) 
        { 
            return next(e); 
        }
        if (docs == null) { 
            var err = new Error('Movie not found');
            err.status = 404;
            return next(e);
        }
        // Success
        res.render('movie_form', { title: 'Update Movie', movie: docs });
    });
};

// Handle Movie update on POST.
exports.movie_update_post = [
    // Validate fields.
    body('title').isLength({ min: 1, max: 50 }).trim().withMessage('Title must be between 1-50 characters.'),
    body('format').isIn(['VHS', 'vhs', 'DVD', 'dvd', 'Streaming', 'streaming']).withMessage('Format must be one of the following: VHS, DVD, or Streaming.'),
    body('length').isInt({ min: 0, max: 500}).trim().withMessage('Length must be a value between 0 - 500.'),
    body('release_year').isInt({ min: 1800, max: 2100 }).trim().withMessage('Release Year must must be a value between 1800 - 2100.'),
    body('rating').isInt({ min: 1, max: 5 }).trim().withMessage('Rating must be a value between 1 - 5.'),

    // Sanitize fields.
    sanitizeBody('title').escape(),
    sanitizeBody('format').escape(),
    sanitizeBody('length').escape(),
    sanitizeBody('release_year').escape(),
    sanitizeBody('rating').escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);
        // Create Movie object with escaped and trimmed data.
        var movie = new Movie(
            {
                title: req.body.title,
                format: req.body.format,
                length: req.body.length,
                release_year: req.body.release_year,
                rating: req.body.rating,
                _id:req.params.id
            }
        );
        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values and error messages.
            res.render('movie_form', { title: 'Update Movie', movie: movie, errors: errors.array() });
            return;
        }
        else {
            //Form is valid. Update the record.
            Movie.findByIdAndUpdate(req.params.id, movie, {}, function (e, docs) {
                if (e) { 
                    return next(e); 
                }
                // Success
                res.redirect(docs.url);
            });
        }
    }
];